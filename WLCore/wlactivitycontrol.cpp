#include "wlactivitycontrol.h"

namespace WL
{
   namespace Core
   {  
      void WLActivityControl::SetProgress(double progress) const
      {
         emit ProgressChanged(progress);
      }

      void WLActivityControl::SetMessage(const QString& message) const
      {
         emit MessageChanged(message);
      }

      void WLActivityControl::SetStatusDescription(const QString& status) const
      {
         emit StatusDescriptionChanged(status);
      }
   }
}