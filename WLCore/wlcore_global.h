#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(WLCORE_LIB)
#  define WLCORE_EXPORT Q_DECL_EXPORT
# else
#  define WLCORE_EXPORT Q_DECL_IMPORT
# endif
#else
# define WLCORE_EXPORT
#endif
