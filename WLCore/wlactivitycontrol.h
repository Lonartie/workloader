#pragma once

#include "stdafx.h"

namespace WL
{
   namespace Core
   {
      class WLActivityControl : public Common::WLObject
      {
         Q_OBJECT;

         MEMORY(WLActivityControl);

         virtual void SetProgress(double progress) const;

         virtual void SetMessage(const QString& message) const;

         virtual void SetStatusDescription(const QString& status) const;

      signals:

         void ProgressChanged(double progress) const;

         void MessageChanged(const QString& message) const;

         void StatusDescriptionChanged(const QString& status) const;

      };
   }
}
