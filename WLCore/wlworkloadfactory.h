#pragma once

#include "stdafx.h"

#include "wlworkload.h"
#include "wlworkloaddefinition.h"

namespace WL
{
   namespace Core
   {
      class WLWorkLoadFactory : public Common::WLObject
      {
         static WLWorkLoad::UPtr CreateWorkLoad(const WLWorkLoadDefinition& WorkLoadDefinition);
      };
   }
}