#pragma once

#include "stdafx.h"
#include "../WLCommon/WLSerializable.h"

#include <QObject>

namespace WL
{
   namespace Core
   {
      class WLTypeRegistrar
      {
      public:
         std::vector<std::type_info> types;
      };

      struct WLPackageAttributes
      {
         bool is_message;
         bool is_data;
         bool is_object;

         std::type_info object_type;
      };

      class WLNetworkController
      {
      public:

         virtual void SendData(const QByteArray& message) = 0;
         virtual void SendMessage(const QString& message) = 0;
         template<typename T> void SendObject(const Common::Serializable<T>& obj) 
         { SendData(obj.Serialize()); };

         virtual void OnReceivedData(const QByteArray& data) = 0;
         virtual void OnReceivedMessage(const QString& message) = 0;

         virtual ~WLNetworkController() = 0;
      };
   }
}