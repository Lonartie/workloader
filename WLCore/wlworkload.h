#pragma once

#include "stdafx.h"

namespace WL
{
   namespace Core
   {
      class WLCORE_EXPORT WLWorkLoad : public Common::WLObject
      {
         Q_OBJECT

         QOBJECT_DEFINITION(WLWorkLoad);

         ~WLWorkLoad() = default;

      };
   }
}
