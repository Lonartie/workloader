#define BOOST_TEST_MODULE LinqTests
#include <boost/test/unit_test.hpp>
#include "main.h"

std::ostream& operator<<(std::ostream& stream, decltype(typeid(void)) type_id)
{ return (stream << type_id.name()); }

std::ostream& operator<<(std::ostream& stream, const QString& str)
{ return (stream << str.toStdString()); }