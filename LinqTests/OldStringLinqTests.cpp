#include "main.h"
#include <string>

BOOST_AUTO_TEST_SUITE(TS_OldStringLinqTests)

BOOST_AUTO_TEST_CASE(Linq_OldStringLinq_ExpectedTypesAreOldStringLinq)
{
   BOOST_CHECK_EQUAL(true, Linq<char*>().IsLinqGroup<OldStringLinq<char*>>());
   BOOST_CHECK_EQUAL(true, Linq<const char*>().IsLinqGroup<OldStringLinq<const char*>>());
   BOOST_CHECK_EQUAL(true, Linq<std::string>().IsLinqGroup<OldStringLinq<std::string>>());
   BOOST_CHECK_EQUAL(false, Linq<char>().IsLinqGroup<OldStringLinq<char>>());
}

BOOST_AUTO_TEST_CASE(Linq_OldStringLinq_FunctionsReturnExpectedTypes)
{
   auto dummy1 = CreateLinq<std::string>({"hello", "world", "!"});
   auto dummy2 = CreateLinq<char*>({"hello", "world", "!"});
   auto dummy3 = CreateLinq<const char*>({"hello", "world", "!"});

   BOOST_CHECK_EQUAL(typeid(Linq<QString>), typeid(dummy1.ToQStringLinq()));
   BOOST_CHECK_EQUAL(typeid(Linq<QString>), typeid(dummy2.ToQStringLinq()));
   BOOST_CHECK_EQUAL(typeid(Linq<QString>), typeid(dummy3.ToQStringLinq()));
}

BOOST_AUTO_TEST_CASE(Linq_OldStringLinq_OldStringFunctions)
{
   auto dummy1 = CreateLinq<std::string>({"hello", "world", "!"});
   auto dummy2 = CreateLinq<char*>({"hello", "world", "!"});
   auto dummy3 = CreateLinq<const char*>({"hello", "world", "!"});

   BOOST_CHECK_NO_THROW(auto _t = dummy1.ToQStringLinq());
   BOOST_CHECK_NO_THROW(auto _t = dummy2.ToQStringLinq());
   BOOST_CHECK_NO_THROW(auto _t = dummy3.ToQStringLinq());

   BOOST_CHECK_EQUAL("helloworld!", dummy1.ToQStringLinq().Concatenate());
   BOOST_CHECK_EQUAL("helloworld!", dummy2.ToQStringLinq().Concatenate());
   BOOST_CHECK_EQUAL("helloworld!", dummy3.ToQStringLinq().Concatenate());
}

BOOST_AUTO_TEST_SUITE_END()