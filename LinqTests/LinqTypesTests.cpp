#include "main.h"
#include <memory>
#include <string>

BOOST_AUTO_TEST_SUITE(TS_LinqTypesTests)

BOOST_AUTO_TEST_CASE(Linq_Ptr)
{
   auto u = new std::string("hello world!");
   auto list(CreateLinq({u, u, u}));
   auto copy = list.Copy();
   auto moved = list.Move();
   auto& ref = moved;

   // check move operator
   BOOST_CHECK_EQUAL(0, list.size());

   copy.Resize(1);

   BOOST_CHECK_EQUAL(3, moved.size());
   BOOST_CHECK_EQUAL(3, ref.size());
   BOOST_CHECK_EQUAL(1, copy.size());

   auto u2 = new std::string("test");
   copy.First() = u2;

   BOOST_CHECK_EQUAL(std::string("test"), *copy.First());
   BOOST_CHECK_EQUAL(std::string("hello world!"), *moved.First());
   BOOST_CHECK_EQUAL(std::string("hello world!"), *ref.First());

   auto u3 = new std::string("test2");
   ref.First() = u3;

   BOOST_CHECK_EQUAL(std::string("test"), *copy.First());
   BOOST_CHECK_EQUAL(std::string("test2"), *moved.First());
   BOOST_CHECK_EQUAL(std::string("test2"), *ref.First());

   delete u, u2, u3;
}

BOOST_AUTO_TEST_CASE(Linq_SharedPtr)
{
   auto u = std::make_shared<std::string>(std::string("hello world!"));
   auto list(CreateLinq({u, u, u}));
   auto copy = list.Copy();
   auto moved = list.Move();
   auto& ref = moved;

   // check move operator
   BOOST_CHECK_EQUAL(0, list.size());

   copy.Resize(1);

   BOOST_CHECK_EQUAL(3, moved.size());
   BOOST_CHECK_EQUAL(3, ref.size());
   BOOST_CHECK_EQUAL(1, copy.size());

   copy.First() = std::make_shared<std::string>(std::string("test"));

   BOOST_CHECK_EQUAL(std::string("test"), *copy.First());
   BOOST_CHECK_EQUAL(std::string("hello world!"), *moved.First());
   BOOST_CHECK_EQUAL(std::string("hello world!"), *ref.First());

   ref.First() = std::make_shared<std::string>(std::string("test2"));

   BOOST_CHECK_EQUAL(std::string("test"), *copy.First());
   BOOST_CHECK_EQUAL(std::string("test2"), *moved.First());
   BOOST_CHECK_EQUAL(std::string("test2"), *ref.First());
}

BOOST_AUTO_TEST_CASE(Linq_UniquePtr)
{
   using StringUPtr = std::unique_ptr<std::string>;

   std::vector<StringUPtr> _list;

   _list.emplace_back(std::make_unique<std::string>("hello world!"));
   _list.emplace_back(std::make_unique<std::string>("hello world!"));
   _list.emplace_back(std::make_unique<std::string>("hello world!"));

   Linq<StringUPtr> list(std::move(_list));

   // unique ptr copy is deleted!

   auto moved = list.Move();
   auto& ref = moved;

   // check move operator
   BOOST_CHECK_EQUAL(0, list.size());

   ref.First() = std::make_unique<std::string>("test2");

   BOOST_CHECK_EQUAL(std::string("test2"), *moved.First());
   BOOST_CHECK_EQUAL(std::string("test2"), *ref.First());
}

BOOST_AUTO_TEST_SUITE_END();