#pragma once
#include <boost/test/unit_test.hpp>
#include "../Playground/Linq.h"
#include <functional>
#include <chrono>
using namespace Common;

#define BOOST_EXTENSION_EQUAL_COLLECTIONS(a, b) BOOST_CHECK_EQUAL_COLLECTIONS(a.cbegin(), a.cend(), b.cbegin(), b.cend())

std::ostream& operator<<(std::ostream& stream, decltype(typeid(void)) type_id);
std::ostream& operator<<(std::ostream& stream, const QString& str);

template<typename U, typename T>
bool PolymorphicTypeOf(T& obj)
{ return dynamic_cast<U*>(&obj) != nullptr; }

template<typename CHRONO_DURATION_TYPE>
auto Measure(std::function<void()> fnc)
{
   using namespace std::chrono;
   auto begin = system_clock::now();
   fnc();
   auto end = system_clock::now();
   return duration_cast<CHRONO_DURATION_TYPE>(end - begin).count();
}
