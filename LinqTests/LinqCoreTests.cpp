#include "main.h"
#include <string>
#include <sstream>

BOOST_AUTO_TEST_SUITE(TS_LinqCore);

BOOST_AUTO_TEST_CASE(Linq_CreateLinq_ReturnsCorrectType)
{
   BOOST_CHECK_EQUAL(typeid(Linq<int>), typeid(CreateLinq<int>()));
   BOOST_CHECK_EQUAL(typeid(Linq<double>), typeid(CreateLinq({1.4, 9.7})));
   BOOST_CHECK_EQUAL(typeid(Linq<std::string>), typeid(CreateLinq<std::string>()));
   BOOST_CHECK_EQUAL(typeid(Linq<const char*>), typeid(CreateLinq({"io", "8"})));
   BOOST_CHECK_EQUAL(typeid(Linq<char>), typeid(CreateLinq({'i', '8'})));
   BOOST_CHECK_EQUAL(typeid(Linq<Linq<int>>), typeid(CreateLinq<Linq<int>>()));
}

BOOST_AUTO_TEST_CASE(Linq_CreateLinq_ReturnsCorrectList)
{
   auto list1(CreateLinq({7, 8, 3, 4}));
   auto ref1(std::vector<int>{7, 8, 3, 4});
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref1, list1);

   auto list2(CreateLinq<std::string>({"hello", "world", "!"}));
   auto ref2(std::vector<std::string>{"hello", "world", "!"});
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref2, list2);

   auto list3(CreateLinq({'i', 'u', 'p', '9', 'a'}));
   auto ref3(std::vector<char>{'i', 'u', 'p', '9', 'a'});
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref3, list3);
}

BOOST_AUTO_TEST_CASE(Linq_Fill_GetRange)
{
   auto list(CreateLinq<int>().Fill(2, 10));
   BOOST_CHECK_EQUAL(10, list.size());
   for (const auto& item : list)
      BOOST_CHECK_EQUAL(2, item);

   auto list2(CreateLinq({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
   auto result2(list2.GetRange(2, 3));
   auto ref(CreateLinq({3, 4, 5}));
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref, result2);
}

BOOST_AUTO_TEST_CASE(Linq_Left)
{
   auto list(CreateLinq({1, 2, 3, 4, 5, 6, 7, 8}));
   auto test1(list.Left(4));
   auto ref1(CreateLinq({1, 2, 3, 4}));
   auto test2(list.Left(3));
   auto ref2(CreateLinq({1, 2, 3}));
   auto test3(list.Left(2));
   auto ref3(CreateLinq({1, 2}));

   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref1, test1);
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref2, test2);
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref3, test3);
}

BOOST_AUTO_TEST_CASE(Linq_Right)
{
   auto list(CreateLinq({1, 2, 3, 4, 5, 6, 7, 8}));
   auto test1(list.Right(4));
   auto ref1(CreateLinq({5, 6, 7, 8}));
   auto test2(list.Right(3));
   auto ref2(CreateLinq({6, 7, 8}));
   auto test3(list.Right(2));
   auto ref3(CreateLinq({7, 8}));

   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref1, test1);
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref2, test2);
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref3, test3);
}

BOOST_AUTO_TEST_CASE(Linq_First)
{
   auto list(CreateLinq<std::string>({"1", "a", "2", "b"}));

   BOOST_CHECK_EQUAL("1", list.First());
   BOOST_CHECK_EQUAL("1", list.FirstOr("0"));
   BOOST_CHECK_EQUAL("1", list.FirstOrDefault());
   BOOST_CHECK_NO_THROW(BOOST_CHECK_EQUAL("1", list.FirstOrThrow(std::exception("empty"))));

   list.Clear();

   //BOOST_CHECK_THROW(list.First(), std::exception); // can't catch assertions... but validity is checked with 'FirstOrThrow'
   BOOST_CHECK_EQUAL("0", list.FirstOr("0"));
   BOOST_CHECK(list.FirstOrDefault().empty());
   BOOST_CHECK_THROW(list.FirstOrThrow(std::exception("empty")), std::exception);
}

BOOST_AUTO_TEST_CASE(Linq_Last)
{
   auto list(CreateLinq<std::string>({"1", "a", "2", "b"}));

   BOOST_CHECK_EQUAL("b", list.Last());
   BOOST_CHECK_EQUAL("b", list.LastOr("0"));
   BOOST_CHECK_EQUAL("b", list.LastOrDefault());
   BOOST_CHECK_NO_THROW(BOOST_CHECK_EQUAL("b", list.LastOrThrow(std::exception("empty"))));

   list.Clear();

   //BOOST_CHECK_THROW(list.Last(), std::exception); // can't catch assertions... but validity is checked with 'LastOrThrow'
   BOOST_CHECK_EQUAL("0", list.LastOr("0"));
   BOOST_CHECK(list.LastOrDefault().empty());
   BOOST_CHECK_THROW(list.LastOrThrow(std::exception("empty")), std::exception);
}

BOOST_AUTO_TEST_CASE(Linq_At)
{
   auto list(CreateLinq<std::string>({"1", "a", "2", "b"}));

   BOOST_CHECK_EQUAL("1", list.At(0));
   BOOST_CHECK_EQUAL("1", list.AtOr(0, "0"));
   BOOST_CHECK_EQUAL("a", list.AtOrDefault(1));
   BOOST_CHECK_NO_THROW(BOOST_CHECK_EQUAL("a", list.AtOrThrow(1, std::exception("empty"))));

   list.Clear();

   //BOOST_CHECK_THROW(list.At(2), std::exception); // can't catch assertions... but validity is checked with 'AtOrThrow'
   BOOST_CHECK_EQUAL("0", list.AtOr(2, "0"));
   BOOST_CHECK(list.AtOrDefault(3).empty());
   BOOST_CHECK_THROW(list.AtOrThrow(3, std::exception("empty")), std::exception);
}

/*
   #################### Operators ####################
*/

BOOST_AUTO_TEST_CASE(Linq_Operator_Equality)
{
   const auto list(CreateLinq<int>({1, 2, 3, 4, 5, 6, 7}));
   const auto list2(CreateLinq<int>({1, 2, 3, 4, 5, 6, 7}));
   const auto list3(CreateLinq<double>({1, 2, 3, 4, 5, 6, 7}));
   const auto list4(CreateLinq<int>({1, 2, 6, 7}));

   BOOST_CHECK(list == list2); // same type, same collection
   BOOST_CHECK(list == list3); // different type, same collection
   BOOST_CHECK(list != list4); // same type, different collection

   BOOST_CHECK_EQUAL(true, list.IsSameType(list2));  // same type
   BOOST_CHECK_EQUAL(false, list.IsSameType(list3)); // different type
   BOOST_CHECK_EQUAL(true, list.IsSameType(list4));  // same type
}

BOOST_AUTO_TEST_CASE(Linq_SetTo)
{
   // same types
   {
      auto list(CreateLinq({1, 2, 3}));
      bool equal;

      BOOST_CHECK((list == std::vector<int> {1, 2, 3}));
      BOOST_CHECK((list == std::initializer_list<int> {1, 2, 3}));
      BOOST_CHECK((list == list));
      BOOST_CHECK((list != std::vector<int> {1, 1, 1}));
      BOOST_CHECK((list != std::initializer_list<int> {1, 1, 1}));

      list = {1, 2, 3, 4};
      std::initializer_list<int> _t = {1, 2, 3, 4};
      list = _t;

      BOOST_CHECK((list == std::vector<int> {1, 2, 3, 4}));
      BOOST_CHECK((list == std::initializer_list<int> {1, 2, 3, 4}));
      BOOST_CHECK((list == list));
      BOOST_CHECK((list != std::vector<int> {1, 1, 1}));
      BOOST_CHECK((list != std::initializer_list<int> {1, 1, 1}));

      list = std::vector<int>{1, 2};
      std::vector<int> _u = {1, 2};
      list = _u;

      BOOST_CHECK((list == std::vector<int> {1, 2}));
      BOOST_CHECK((list == std::initializer_list<int> {1, 2}));
      BOOST_CHECK((list == list));
      BOOST_CHECK((list != std::vector<int> {1, 1, 1}));
      BOOST_CHECK((list != std::initializer_list<int> {1, 1, 1}));
   }
   // different types
   {
      auto list(CreateLinq({1, 2, 3}));
      bool equal;

      BOOST_CHECK((list == std::vector<long> {1, 2, 3}));
      BOOST_CHECK((list == std::initializer_list<unsigned> {1, 2, 3}));
      BOOST_CHECK((list == list));
      BOOST_CHECK((list != std::vector<int> {1, 1, 1}));
      BOOST_CHECK((list != std::initializer_list<int> {1, 1, 1}));

      list = {1, 2, 3, 4};
      std::initializer_list<int> _t = {1, 2, 3, 4};
      list = _t;

      BOOST_CHECK((list == std::vector<wchar_t> {1, 2, 3, 4}));
      BOOST_CHECK((list == std::initializer_list<std::size_t> {1, 2, 3, 4}));
      BOOST_CHECK((list == list));
      BOOST_CHECK((list != std::vector<float> {1, 1, 1}));
      BOOST_CHECK((list != std::initializer_list<double> {1, 1, 1}));

      list = std::vector<int>{1, 2};
      std::vector<int> _u = {1, 2};
      list = _u;

      BOOST_CHECK((list == std::vector<float> {1, 2}));
      BOOST_CHECK((list == std::initializer_list<double> {1, 2}));
      BOOST_CHECK((list == list));
      BOOST_CHECK((list != std::vector<wchar_t> {1, 1, 1}));
      BOOST_CHECK((list != std::initializer_list<unsigned char> {1, 1, 1}));
   }
}

BOOST_AUTO_TEST_CASE(Linq_AssignmentOperators)
{
   auto list(CreateLinq<int>().FillRange(20));
   std::vector<double> assign{1, 2, 3};

   BOOST_CHECK_EQUAL(20, list.size());
   BOOST_CHECK_EQUAL(0, list.First());
   BOOST_CHECK_EQUAL(19, list.Last());
}

/*
   #################### Main functions ####################
*/

BOOST_AUTO_TEST_CASE(Linq_Select)
{
   auto list(CreateLinq<std::size_t>().FillRange(20));

   auto selector_1 = [](const auto& a) { return std::to_string(a); };
   auto& selector_1_Select = list.Select(selector_1);
   auto& selector_1_AutoSelect = list.AutoSelect(selector_1);

   BOOST_CHECK_EQUAL(typeid(Linq<std::string>), typeid(selector_1_Select));                  // Check result type
   BOOST_CHECK_EQUAL(typeid(Linq<std::string>), typeid(selector_1_AutoSelect));              // Check result type
   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&selector_1_Select));         // Check copy or reference
   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&selector_1_AutoSelect));     // Check copy or reference

   BOOST_CHECK_EQUAL(20, selector_1_Select.size());
   BOOST_CHECK_EQUAL(20, selector_1_AutoSelect.size());

   auto selector_2 = [](const auto& a) { return a * a; };
   auto& selector_2_Select = list.Select(selector_2);
   auto& selector_2_AutoSelect = list.AutoSelect(selector_2);
   auto& selector_2_SelectInPlace = list.SelectInPlace(selector_2);

   BOOST_CHECK_EQUAL(typeid(Linq<std::size_t>), typeid(selector_2_Select));                  // Check result type
   BOOST_CHECK_EQUAL(typeid(Linq<std::size_t>), typeid(selector_2_AutoSelect));              // Check result type
   BOOST_CHECK_EQUAL(typeid(Linq<std::size_t>), typeid(selector_2_SelectInPlace));           // Check result type
   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&selector_2_Select));         // Check copy or reference
   BOOST_CHECK(static_cast<void*>(&list) == static_cast<void*>(&selector_2_AutoSelect));     // Check copy or reference (when type not changed, no need for copy list)
   BOOST_CHECK(static_cast<void*>(&list) == static_cast<void*>(&selector_2_SelectInPlace));  // Check copy or reference

   BOOST_CHECK_EQUAL(20, selector_2_Select.size());
   BOOST_CHECK_EQUAL(20, selector_2_AutoSelect.size());
   BOOST_CHECK_EQUAL(20, selector_2_SelectInPlace.size());
}

BOOST_AUTO_TEST_CASE(Linq_SelectIf)
{
   auto list(CreateLinq<std::size_t>().FillRange(20));

   auto selector_1 = [](const auto& a) { return std::to_string(a); };
   auto condition_1 = [](const auto& a) { return a.size() > 1; };
   auto& selector_1_Select = list.SelectIf(selector_1, condition_1);
   auto& selector_1_AutoSelect = list.AutoSelectIf(selector_1, condition_1);

   BOOST_CHECK_EQUAL(typeid(Linq<std::string>), typeid(selector_1_Select));                  // Check result type
   BOOST_CHECK_EQUAL(typeid(Linq<std::string>), typeid(selector_1_AutoSelect));              // Check result type
   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&selector_1_Select));         // Check copy or reference
   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&selector_1_AutoSelect));     // Check copy or reference

   BOOST_CHECK_EQUAL(10, selector_1_Select.size());
   BOOST_CHECK_EQUAL(10, selector_1_AutoSelect.size());

   auto selector_2 = [](const auto& a) { return a * a; };
   auto condition_2 = [](const auto& a) { return a > 9; };
   auto& selector_2_Select = list.SelectIf(selector_2, condition_2);
   auto& selector_2_AutoSelect = list.AutoSelectIf(selector_2, condition_2);
   auto& selector_2_SelectInPlace = list.SelectInPlaceIf(selector_2, condition_2);

   BOOST_CHECK_EQUAL(typeid(Linq<std::size_t>), typeid(selector_2_Select));                  // Check result type
   BOOST_CHECK_EQUAL(typeid(Linq<std::size_t>), typeid(selector_2_AutoSelect));              // Check result type
   BOOST_CHECK_EQUAL(typeid(Linq<std::size_t>), typeid(selector_2_SelectInPlace));           // Check result type
   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&selector_2_Select));         // Check copy or reference
   BOOST_CHECK(static_cast<void*>(&list) == static_cast<void*>(&selector_2_AutoSelect));     // Check copy or reference (when type not changed, no need for copy list)
   BOOST_CHECK(static_cast<void*>(&list) == static_cast<void*>(&selector_2_SelectInPlace));  // Check copy or reference

   BOOST_CHECK_EQUAL(16, selector_2_Select.size());
   BOOST_CHECK_EQUAL(16, selector_2_AutoSelect.size());
   BOOST_CHECK_EQUAL(16, selector_2_SelectInPlace.size());
}

BOOST_AUTO_TEST_CASE(Linq_IfSelect)
{
   auto list(CreateLinq<std::size_t>().FillRange(20));

   auto selector_1 = [](const auto& a) { return std::to_string(a); };
   auto condition_1 = [](const std::size_t a) { return a > 9; };
   auto& selector_1_Select = list.IfSelect(condition_1, selector_1);
   auto& selector_1_AutoSelect = list.AutoIfSelect(condition_1, selector_1);

   BOOST_CHECK_EQUAL(typeid(Linq<std::string>), typeid(selector_1_Select));                  // Check result type
   BOOST_CHECK_EQUAL(typeid(Linq<std::string>), typeid(selector_1_AutoSelect));              // Check result type
   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&selector_1_Select));         // Check copy or reference
   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&selector_1_AutoSelect));     // Check copy or reference

   BOOST_CHECK_EQUAL(10, selector_1_Select.size());
   BOOST_CHECK_EQUAL(10, selector_1_AutoSelect.size());

   auto selector_2 = [](const auto& a) { return a * a; };
   auto condition_2 = [](const auto& a) { return a > 9; };
   auto& selector_2_Select = list.IfSelect(condition_2, selector_2);
   auto& selector_2_AutoSelect = list.AutoIfSelect(condition_2, selector_2);
   auto& selector_2_SelectInPlace = list.IfSelectInPlace(condition_2, selector_2);

   BOOST_CHECK_EQUAL(typeid(Linq<std::size_t>), typeid(selector_2_Select));                  // Check result type
   BOOST_CHECK_EQUAL(typeid(Linq<std::size_t>), typeid(selector_2_AutoSelect));              // Check result type
   BOOST_CHECK_EQUAL(typeid(Linq<std::size_t>), typeid(selector_2_SelectInPlace));           // Check result type
   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&selector_2_Select));         // Check copy or reference
   BOOST_CHECK(static_cast<void*>(&list) == static_cast<void*>(&selector_2_AutoSelect));     // Check copy or reference (when type not changed, no need for copy list)
   BOOST_CHECK(static_cast<void*>(&list) == static_cast<void*>(&selector_2_SelectInPlace));  // Check copy or reference

   BOOST_CHECK_EQUAL(10, selector_2_Select.size());
   BOOST_CHECK_EQUAL(10, selector_2_AutoSelect.size());
   BOOST_CHECK_EQUAL(10, selector_2_SelectInPlace.size());
}

BOOST_AUTO_TEST_CASE(Linq_Where)
{
   auto list(CreateLinq<int>().FillRange(20));
   auto even = [](const auto& x) { return x % 2 == 0; };
   auto odd = [](const auto& x) { return x % 2 != 0; };
   auto even_list = list.Copy().Where(even);
   auto& odd_list = list.Where(odd);

   auto ref_even(CreateLinq<int>().FillRange(0, 20, 2));
   auto ref_odd(CreateLinq<int>().FillRange(1, 20, 2));

   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref_even, even_list);
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref_odd, odd_list);

   BOOST_CHECK(static_cast<void*>(&list) != static_cast<void*>(&even_list));                 // Check for copy or reference
   BOOST_CHECK(static_cast<void*>(&list) == static_cast<void*>(&odd_list));                  // Check for copy or reference (Where should return reference!)
}

BOOST_AUTO_TEST_CASE(Linq_Size_Count)
{
   auto list(CreateLinq<int>().FillRange(20));

   BOOST_CHECK_EQUAL(20, list.size());
   BOOST_CHECK_EQUAL(1, list.Count(0));
   BOOST_CHECK_EQUAL(1, list.Count(1));
   BOOST_CHECK_EQUAL(1, list.Count(19));
   BOOST_CHECK_EQUAL(0, list.Count(20));

   std::vector<int> ref{0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
   list.RemoveAt(2);

   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref, list);
   BOOST_CHECK_EQUAL(19, list.size());
   BOOST_CHECK_EQUAL(1, list.Count(1));
   BOOST_CHECK_EQUAL(0, list.Count(2));
   BOOST_CHECK_EQUAL(1, list.Count(3));

   ref = {0, 1, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
   list.Remove(7);

   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref, list);
   BOOST_CHECK_EQUAL(18, list.size());
   BOOST_CHECK_EQUAL(1, list.Count(6));
   BOOST_CHECK_EQUAL(0, list.Count(7));
   BOOST_CHECK_EQUAL(1, list.Count(8));

   ref = {0, 1, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2};
   list.Add(2);

   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref, list);
   BOOST_CHECK_EQUAL(19, list.size());
   BOOST_CHECK_EQUAL(1, list.Count(2));

   ref = {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2};
   list.AddAt(2, 2);

   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref, list);
   BOOST_CHECK_EQUAL(20, list.size());
   BOOST_CHECK_EQUAL(2, list.Count(2));
   BOOST_CHECK_EQUAL(2, list[2]);
}

BOOST_AUTO_TEST_SUITE_END();