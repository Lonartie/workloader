#include "main.h"
#include <QtCore/QString>
#include <string>

BOOST_AUTO_TEST_SUITE(TS_QStringLinqTests)

BOOST_AUTO_TEST_CASE(Linq_QStringLinq_ExpectedTypesAreQStringLinq)
{
   BOOST_CHECK_EQUAL(true, Linq<QString>().IsLinqGroup<QStringLinq<QString>>());
   BOOST_CHECK_EQUAL(false, Linq<std::string>().IsLinqGroup<QStringLinq<std::string>>());
}

BOOST_AUTO_TEST_CASE(Linq_QStringLinq_FunctionsReturnExpectedTypes)
{
   auto dummy = CreateLinq<QString>();

   BOOST_CHECK_EQUAL(typeid(Linq<int>), typeid(dummy.ToIntLinq()));
   BOOST_CHECK_EQUAL(typeid(Linq<float>), typeid(dummy.ToFloatLinq()));
   BOOST_CHECK_EQUAL(typeid(Linq<double>), typeid(dummy.ToDoubleLinq()));
   BOOST_CHECK_EQUAL(typeid(QStringList), typeid(dummy.ToQStringList()));
   BOOST_CHECK_EQUAL(typeid(QString), typeid(dummy.Concatenate()));
}

BOOST_AUTO_TEST_CASE(Linq_QStringLinq_QStringFunctions)
{
   const auto list = CreateLinq<QString>({"a", "b", "c", "d", "e"});
   const auto CommaSeparatedString = [list](auto& a, const auto& b) { a += b + (b != list.Last() ? ", " : ""); };

   BOOST_CHECK_EQUAL("abcde", list.Concatenate());
   BOOST_CHECK_EQUAL("a, b, c, d, e", list.Accumulate(QString(), CommaSeparatedString));

   const auto ToQString = [](auto num) { return QString::number(num); };
   const auto nums = CreateLinq<QString>().FillRangeFunction(5, ToQString);

   const std::vector<int> ref_int{0, 1, 2, 3, 4};
   const std::vector<float> ref_float{0, 1, 2, 3, 4};
   const std::vector<double> ref_double{0, 1, 2, 3, 4};

   const auto int_result = nums.ToIntLinq();
   const auto float_result = nums.ToFloatLinq();
   const auto double_result = nums.ToDoubleLinq();

   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref_int, int_result);
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref_float, float_result);
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref_double, double_result);
}

BOOST_AUTO_TEST_SUITE_END()