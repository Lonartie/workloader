#include "main.h"
#include <string>

BOOST_AUTO_TEST_SUITE(TS_CharLinqTests)

BOOST_AUTO_TEST_CASE(Linq_CharLinq_ExpectedTypesAreCharLinq)
{
   BOOST_CHECK_EQUAL(true, Linq<char>().IsLinqGroup<CharLinq<char>>());
   BOOST_CHECK_EQUAL(true, Linq<QChar>().IsLinqGroup<CharLinq<QChar>>());
   BOOST_CHECK_EQUAL(false, Linq<int>().IsLinqGroup<CharLinq<int>>());
}

BOOST_AUTO_TEST_CASE(Linq_CharLinq_FunctionsReturnExpectedTypes)
{}

BOOST_AUTO_TEST_CASE(Linq_CharLinq_OldStringFunctions)
{}

BOOST_AUTO_TEST_SUITE_END()