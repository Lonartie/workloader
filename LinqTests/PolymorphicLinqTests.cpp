#include "main.h"
#include "TestLinqGroup.h"

BOOST_AUTO_TEST_SUITE(TS_PolymorphicLinq)

BOOST_AUTO_TEST_CASE(Linq_GroupCreation)
{
   const auto ByID = [](auto& a, auto& b) { return a.ID < b.ID; };

   auto list = CreateLinq<Test_Linq_Struct_int>({{1}, {2},{0},{-1}});
   auto ref_list = CreateLinq({1, 2, 0, -1});
   auto list2(list.GetID());
   auto list3(list.Sort(ByID).GetID());
   auto ref_list2 = CreateLinq({-1, 0, 1, 2});

   BOOST_CHECK_EQUAL(true, PolymorphicTypeOf<Test_Linq<Test_Linq_Struct_int>>(list));
   BOOST_CHECK_EQUAL(true, list.IsTestLinq());
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref_list, list2);
   BOOST_EXTENSION_EQUAL_COLLECTIONS(ref_list2, list3);
}

BOOST_AUTO_TEST_CASE(Linq_Polymorphics)
{
   auto poly(CreateLinq<Test_Linq_Struct_long>());

   // Linq of Test_Linq_Struct_long is of type Linq AND Test_Linq AND NumericLinq!
   BOOST_CHECK_EQUAL(true, poly.IsLinqGroup<Linq<Test_Linq_Struct_long>>());
   BOOST_CHECK_EQUAL(true, poly.IsLinqGroup<Test_Linq<Test_Linq_Struct_long>>());
   BOOST_CHECK_EQUAL(true, poly.IsLinqGroup<NumericLinq<Test_Linq_Struct_long>>());

   auto poly2(CreateLinq<Test_Linq_Struct_int>());

   BOOST_CHECK_EQUAL(true, poly2.IsLinqGroup<Linq<Test_Linq_Struct_int>>());
   BOOST_CHECK_EQUAL(true, poly2.IsLinqGroup<Test_Linq<Test_Linq_Struct_int>>());
   BOOST_CHECK_EQUAL(false, poly2.IsLinqGroup<NumericLinq<Test_Linq_Struct_int>>());
}

BOOST_AUTO_TEST_SUITE_END()