#include <boost/test/unit_test.hpp>
#include "../Playground/Linq.h"
using namespace Common;

#include <sstream>

std::ostream& operator<<(std::ostream& stream, decltype(typeid(void)) type_id)
{ return (stream << type_id.name()); }