#include "main.h"
#include <string>

BOOST_AUTO_TEST_SUITE(TS_NumericLinqTests)

BOOST_AUTO_TEST_CASE(Linq_NumericLinq_ExpectedTypesAreNumericLinq)
{
   BOOST_CHECK_EQUAL(true, Linq<char>().IsLinqGroup<NumericLinq<char>>());
   BOOST_CHECK_EQUAL(true, Linq<int>().IsLinqGroup<NumericLinq<int>>());
   BOOST_CHECK_EQUAL(true, Linq<unsigned>().IsLinqGroup<NumericLinq<unsigned>>());
   BOOST_CHECK_EQUAL(true, Linq<float>().IsLinqGroup<NumericLinq<float>>());
   BOOST_CHECK_EQUAL(true, Linq<double>().IsLinqGroup<NumericLinq<double>>());
   BOOST_CHECK_EQUAL(true, Linq<long double>().IsLinqGroup<NumericLinq<long double>>());
   BOOST_CHECK_EQUAL(true, Linq<long>().IsLinqGroup<NumericLinq<long>>());
   BOOST_CHECK_EQUAL(true, Linq<long long>().IsLinqGroup<NumericLinq<long long>>());
   BOOST_CHECK_EQUAL(true, Linq<unsigned long long>().IsLinqGroup<NumericLinq<unsigned long long>>());
   BOOST_CHECK_EQUAL(false, Linq<std::string>().IsLinqGroup<NumericLinq<std::string>>());
}

BOOST_AUTO_TEST_CASE(Linq_NumericLinq_ResultIsOfTypeT, *boost::unit_test::tolerance(1E-6))
{
   auto int_list = CreateLinq<int>({1, 2, 3});
   auto double_list = CreateLinq<double>({1, 2, 3});

   int_list.Normalized();
   double_list.Normalized();

   BOOST_CHECK_EQUAL(0, int_list[0]);
   BOOST_CHECK_EQUAL(0, int_list[1]);
   BOOST_CHECK_EQUAL(1, int_list[2]);

   BOOST_CHECK_EQUAL(0.2672612419124244, double_list[0]);
   BOOST_CHECK_EQUAL(0.53452248382484879, double_list[1]);
   BOOST_CHECK_EQUAL(0.80178372573727319, double_list[2]);
}

BOOST_AUTO_TEST_CASE(Linq_NumericLinq_NumericFunctions, *boost::unit_test::tolerance(1E-6))
{
   auto list = CreateLinq<double>().FillRange(30);

   BOOST_CHECK_EQUAL(435, list.Sum());
   BOOST_CHECK_EQUAL(15, list.Median());
   BOOST_CHECK_EQUAL(14.5, list.Average());
   BOOST_CHECK_EQUAL(92.493242996448117, list.Magnitude());

   list.Normalized();

   BOOST_CHECK_EQUAL(1, list.Magnitude());
   BOOST_CHECK_EQUAL(0, list.First());
   BOOST_CHECK_EQUAL(0.31353641693711232, list.Last());
   BOOST_CHECK_EQUAL(0.16217400876057533, list.Median());
}

BOOST_AUTO_TEST_SUITE_END()