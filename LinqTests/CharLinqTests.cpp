#include "main.h"
#include <string>

BOOST_AUTO_TEST_SUITE(TS_CharLinqTests)

BOOST_AUTO_TEST_CASE(Linq_CharLinq_ExpectedTypesAreCharLinq)
{
   BOOST_CHECK_EQUAL(true, Linq<char>().IsLinqGroup<CharLinq<char>>());
   BOOST_CHECK_EQUAL(true, Linq<QChar>().IsLinqGroup<CharLinq<QChar>>());
   BOOST_CHECK_EQUAL(false, Linq<int>().IsLinqGroup<CharLinq<int>>());
}

BOOST_AUTO_TEST_CASE(Linq_CharLinq_FunctionsReturnExpectedTypes)
{
   auto list_char = CreateLinq<char>();
   auto list_qchar = CreateLinq<QChar>();

   BOOST_CHECK_EQUAL(typeid(QString), typeid(list_char.ToQString()));
   BOOST_CHECK_EQUAL(typeid(QString), typeid(list_qchar.ToQString()));
}

BOOST_AUTO_TEST_CASE(Linq_CharLinq_OldStringFunctions)
{
   auto list_char = CreateLinq<char>({'a', '1', 'b', '2'});
   auto list_qchar = CreateLinq<QChar>({'a', 'b', '1', '2'});

   BOOST_CHECK_EQUAL("a1b2", list_char.ToQString());
   BOOST_CHECK_EQUAL("ab12", list_qchar.ToQString());
}

BOOST_AUTO_TEST_SUITE_END()