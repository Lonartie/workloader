#pragma once
#include "../Playground/LinqSpecializationMaker.h"
#include "../Playground/NumericLinq.h"

struct Test_Linq_Struct_int
{
   int ID;
};
struct Test_Linq_Struct_long
{
   long ID;
};

namespace Common
{
   LINQ_GROUP_BEGIN(Test_Linq);

   const std::type_info& GetTypeInside()
   {
      return typeid(T);
   }

   bool IsTestLinq()
   {
      return true;
   }

   auto GetID()
   {
      auto Select_ID = [](const auto& x) { return x.ID; };
      return This().Select(Select_ID);
   }

   LINQ_GROUP_END(Test_Linq);

   LINQ_SPECIALIZATION(Test_Linq_Struct_int, Test_Linq);

   LINQ_MULTI_SPECIALIZATION(Test_Linq_Struct_long,
                             public Test_Linq<Test_Linq_Struct_long>,
                             public NumericLinq<Test_Linq_Struct_long>);
}