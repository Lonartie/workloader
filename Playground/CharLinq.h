#pragma once
#include "LinqSpecializationMaker.h"

namespace Common
{
   /*
      ##################### Specialization group #####################
   */

   LINQ_GROUP_BEGIN(CharLinq);

   /// @brief 							get the elements as QString
   /// @note  							ONLY IF T IS QCHAR OR CHAR
   QString ToQString()
   {
      return This().Accumulate(QString(), [](auto& _str, const auto& _char)
      {
         return _str += QChar(_char);
      });
   }

   LINQ_GROUP_END(CharLinq);

   /*
      ##################### Specializations #####################
   */

   LINQ_SPECIALIZATION(QChar, CharLinq);
}