#pragma once

#include "NumericLinq.h"
#include "CharLinq.h"

namespace Common
{
   /*
      ##################### NumericLinq AND CharLinq #####################
   */

   LINQ_MULTI_SPECIALIZATION(char,
                             public NumericLinq<char>,
                             public CharLinq<char>);
}