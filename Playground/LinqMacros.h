#pragma once

#define FUNCTION(...) [&] (__VA_ARGS__)

#define MEMBER_FUNCTION(name, ...) FUNCTION(auto& x) { return x.name(__VA_ARGS__); }
#define MEMBER_FUNCTION_PTR(name, ...) FUNCTION(auto& x) { return x->name(__VA_ARGS__); }

#define Lambda1(a, func) FUNCTION(auto &a) { return (func) ; }
#define Lambda2(a, b, func) FUNCTION(auto &a, auto &b) { return (func) ; }
#define Lambda3(a, b, c, func) FUNCTION(auto &a, auto &b, auto &c) { return (func) ; }
#define Lambda4(a, b, c, d, func) FUNCTION(auto &a, auto &b, auto &c, auto &d) { return (func) ; }
#define LAMBDA(args, func) FUNCTION args { return (func); }

//#define GLOBAL_CAST(ret_type, ...) static_cast<ret_type(*)(__VA_ARGS__)>
//#define OBJECT_CAST(ret_type, _class, ...) static_cast<ret_type(_class::*)(__VA_ARGS__)>

#define LINQ_GROUP_BEGIN(class_name) template<typename T> struct class_name : public LinqData<T> {
#define LINQ_GROUP_END(class_name) public: virtual ~##class_name () {} }; template<typename T> struct remove_container<class_name<T>> { using type = T; };
#define LINQ_GROUP_END_CUSTOM_DESTRUCTOR(class_name) }; template<typename T> struct remove_container<class_name<T>> { using type = T; };

#include <iostream>

/// TODO: assignment operators to template functions!

#define _LINQ_MULTI_SPECIALIZATION(type, ...)                                                                  \
 final : public BaseLinq<type>, __VA_ARGS__                                                                    \
{                                                                                                              \
   using BaseLinq<type>::BaseLinq;                                                                             \
   Linq() : BaseLinq<type>() { }                                                                               \
   Linq(const Linq<type>& list) : BaseLinq<type>(list) {}                                                      \
   Linq(Linq<type>&& list) : BaseLinq<type>(std::move(list)) {}                                                \
   virtual std::vector<type>& List() override { return BaseLinq<type>::List(); }                               \
   virtual const std::vector<type>& ConstList() const override { return BaseLinq<type>::ConstList(); }         \
   virtual Linq<type>& This() override { return *this; }                                                       \
   virtual const Linq<type>& ConstThis() const override { return *this; }                                      \
   /*SetTo - Operators*/                                                                                       \
   Linq<type>& operator=(const Linq<type>& list) noexcept                                                      \
   { m_list = list.ConstList(); return *this; }                                                                \
   Linq<type>& operator=(Linq<type>&& list) noexcept                                                           \
   { m_list = std::move(list.List()); return *this; }                                                          \
   Linq<type>& operator=(const std::vector<type>& list) noexcept                                               \
   { m_list = list; return *this;}                                                                             \
   Linq<type>& operator=(std::vector<type>&& list) noexcept                                                    \
   { m_list = std::move(list); return *this; }                                                                 \
   Linq<type>& operator=(const std::initializer_list<type>& list) noexcept                                     \
   { m_list = std::vector<type>(list); return *this; }                                                         \
   Linq<type>& operator=(std::initializer_list<type>&& list) noexcept                                          \
   { m_list = std::move(std::vector<type>(std::move(list))); return *this; }                                   \
   /*Equals - Operators*/                                                                                      \
   template<typename U> bool operator==(const Linq<U>& list) const                                             \
   { return BaseLinq<type>::operator==<U>(list); }                                                             \
   template<typename U> bool operator!=(const Linq<U>& list) const                                             \
   { return !operator==<U>(list); }                                                                            \
   template<typename U> bool operator==(const std::vector<U>& list) const                                      \
   { return BaseLinq<type>::operator==<U>(list); }                                                             \
   template<typename U> bool operator!=(const std::vector<U>& list) const                                      \
   { return !operator==<U>(list); }                                                                            \
   template<typename U> bool operator==(const std::initializer_list<U>& list) const                            \
   { return BaseLinq<type>::operator==<U>(list); }                                                             \
   template<typename U> bool operator!=(const std::initializer_list<U>& list) const                            \
   { return !operator==<U>(list); }                                                                            \
                                                                                                               \
   virtual ~Linq() = default;                                                                                  \
};

#define LINQ_MULTI_SPECIALIZATION(type, ...)                                                                   \
template<> struct Linq<type> _LINQ_MULTI_SPECIALIZATION(type, __VA_ARGS__)
#define LINQ_TEMPLATE_SPECIALIZATION(type, group) struct Linq<type> _LINQ_MULTI_SPECIALIZATION(type, public group<type>)
#define LINQ_SPECIALIZATION(type, group) LINQ_MULTI_SPECIALIZATION(type, public group<type>)
