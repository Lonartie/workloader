#pragma once

#include "LinqMacros.h"
#include "AbstractLinq.h"
#include "LinqStructs.h"
#include "BaseLinq.h"
#include "LinqSpecializations.h"
#include "LinqCreators.h"