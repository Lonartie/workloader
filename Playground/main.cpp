#include "Linq.h"

#include <iostream>
#include <functional>
#include <chrono>
#include <string>

#define print(x) std::cout << x << std::endl
using namespace std::chrono;

extern void Example_CreateLinq();
extern void Example_Manipulate();
extern void Example_Others();
extern void Before_After();
extern void Maximum_Readability();
extern void preparing();
extern void StressNormal();
extern void StressLinq();

long measure(std::function<void()> fnc)
{
   auto begin = system_clock::now();
   fnc();
   auto end = system_clock::now();
   return duration_cast<milliseconds>(end - begin).count();
}

int main()
{
   print("CreateLinq examples [" << measure(&Example_CreateLinq) << " ms]");
   print("Manipulate lists examples [" << measure(&Example_Manipulate) << " ms]");
   print("Other examples [" << measure(&Example_Others) << " ms]");
   print("before-after examples [" << measure(&Before_After) << " ms]");
   print("Maxiumum Readability [" << measure(&Maximum_Readability) << " ms]");

   std::cin.get();
}