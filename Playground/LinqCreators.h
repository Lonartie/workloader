#pragma once

#include "BaseLinq.h"

namespace Common
{
   template<typename T>
   Linq<typename remove_container<T>::type> CreateLinq(const T& input)
   {
      return Linq<remove_container<T>::type>(input);
   };

   template<typename T>
   Linq<T> CreateLinq()
   {
      return Linq<T>();
   };

   template<typename T>
   Linq<T> CreateLinq(const std::initializer_list<T>& input)
   {
      return Linq<T>(input);
   }

   template<typename T>
   Linq<T> CreateLinq(std::initializer_list<T>&& input)
   {
      return Linq<T>(std::move(input));
   }

   template<typename T>
   Linq<T> CreateLinq(T* begin, T* end)
   {
      return Linq<T>(begin, end);
   }

   template<typename T, typename ...Args>
   T Constructor(Args...args)
   {
      return T(args...);
   }
}