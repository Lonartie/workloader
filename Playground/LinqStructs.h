#pragma once

#include "AbstractLinq.h"

#include <QVector>
#include <QStringList>
#include <QString>

#include <vector>

namespace Common
{
   /*
      ##################### remove_container #####################
   */
   template<typename T> struct remove_container { using type = T; };

   template<typename T> struct remove_container<std::vector<T>> { using type = T; };
   template<typename T> struct remove_container<QVector<T>> { using type = T; };
   template<typename T> struct remove_container<std::initializer_list<T>> { using type = T; };
   template<> struct remove_container<QStringList> { using type = QString; };

   /*
      ##################### is_one_of #####################
   */

   template<typename T, typename First, typename ...Rest> struct is_one_of
   { enum { value = std::is_same<T, First>::value || is_one_of<T, Rest...>::value }; };
   template<typename T, typename U> struct is_one_of<T, U>
   { enum { value = std::is_same<T, U>::value }; };

   /*
      ##################### General abstract Linq #####################
   */

   template<typename T>
   struct BaseLinq;

#pragma warning(push)
#pragma warning(disable : 4584)
   template<typename T> struct Linq _LINQ_MULTI_SPECIALIZATION(T, public LinqData<T>);
#pragma warning(pop)

   /*
      ##################### ToLinq #####################
   */

   template<typename T, typename U>
   Linq<T>& ToLinq(U* obj)
   {
      return *static_cast<Linq<T>*>(static_cast<void*>(obj));
   }

   template<typename T, typename U>
   const Linq<T>& ToConstLinq(const U* obj)
   {
      return *static_cast<const Linq<T>*>(static_cast<const void*>(obj));
   }
}