#pragma once
#include "LinqSpecializationMaker.h"

namespace Common
{

   LINQ_GROUP_BEGIN(BaseLinq2D);

   T ToOneLinq() const
   {
      const auto LinqAppender = [](auto& linq, const auto& value) { linq.Merge(value); };
      return ConstThis().Accumulate(T(), LinqAppender);
   }

   T ToOneLinqMove()
   {
      const auto LinqAppender = [](auto& linq, auto&& value) { linq.Merge(value.Move()); };
      auto val = This().AccumulateMove(T(), LinqAppender);
      This().Clear();
      return std::move(val);
   }

   LINQ_GROUP_END(BaseLinq2D);

   template<typename T> LINQ_TEMPLATE_SPECIALIZATION(Linq<T>, BaseLinq2D);
   template<typename T> using Linq2D = Linq<Linq<T>>;

}