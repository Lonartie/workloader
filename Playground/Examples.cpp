#pragma once
#include "Linq.h"

using namespace Common;

void Example_CreateLinq()
{
   Linq<int> li = CreateLinq({1, 2, 3, 4});                             // create Linq from initializer list
   li = CreateLinq(std::vector<int>{1, 2, 3});                          // create Linq from vector
   QStringList list{"1", "2", "3", "4"};
   Linq<QString> lq = CreateLinq(list);                                 // create Linq from QStringList
   li = CreateLinq(QVector<int>{1, 2, 3});                              // create Linq from QVector

   auto lf = CreateLinq<float>().Fill(2, 5);                            // fill a Linq<float> with a 2 five times
   auto ld = CreateLinq<double>().FillRange(2, 0.1, 10);                // fill from 2 increment by 0.1 until 10
   auto lu = lf.Move();                                                 // Move function (does the same as std::move)
   ld = CreateLinq<double>().FillRange(20);                             // fill numbers from 0 to 19
   ld = CreateLinq<double>().FillRangeFunction(10, Lambda1(x, 2 * x));          // fill numbers from 0 to 18 increment by 2

   // Lambda1 - Lambda4 functions are just anonymous functions like [&](auto& a...) { return ... ; }
   // You can always write your own anonymous functions, but I like the short form
   // The last example could be written as follows:
   //
   // ld = CreateLinq<double>().FillRange(10, [&](int index){ return double (2 * x); });
   //
   // or like so:
   //
   // ld = CreateLinq<double>().Fillrange(10, Function(int index) { return double (2 * x); });
}

namespace
{
   QString ToQString(const char* str)
   {
      return QString::fromUtf8(str);
   }
   int ToInt(const QString& str)
   {
      return str.toInt();
   }
   bool IsEven(int x)
   {
      return !(x % 2);
   }
   bool IsOdd(int x)
   {
      return !IsEven(x);
   }
}

void Example_Manipulate()
{
   auto list = CreateLinq({"1", "2" , "3" , "4" , "5"});

   // list is of type Linq<const char*>
   // we want a new list like {1, 3, 5}, every even number as int type
   auto result = list.
      Select(&ToQString).                                               // convert to qstring for toInt method
      SelectIf(&ToInt, &IsOdd);                                         // or 'Select( Member(toInt) )' /  Filter only odd numbers
   // .Get();                                                           // to get the std::vector data

   // result is {1, 3, 5} and of type Linq<int>

   auto list2 = CreateLinq({"1:hello", "2:text", "3:world!"});

   // we want the output string "hello world!"

   auto StringAccumulatorWithSpaceBetween = [](auto& _res, const auto& _str) { _res += (_res.isEmpty() ? "" : " ") + _str; };
   auto StringAsNumIsOdd = [](const auto& str_list) { return str_list[0].toInt() % 2 != 0; };
   auto SelectNthElement = [](const auto& list, int n) { return list[n]; };

   auto result2 = list2.
      Select(&ToQString).                                               // convert to qstring
      SelectIf(MEMBER_FUNCTION(split, ":"),                             // split by ':'
               StringAsNumIsOdd).                                       // take only odd numbers
      Select(SelectNthElement, 1).                                      // select the text
      Accumulate(QString(), StringAccumulatorWithSpaceBetween);         // accumulate the text

   // result2 is "hello world!"

   auto list3 = CreateLinq<std::pair<int, char>>({{0, 'a'}, {1, 'b'}, {5, 'e'}, {4, 'd'}, {2, 'c'}});

   // we want to sort the list by its member n and get the result string "abcde"

   auto IntMember_Comparer = [](const auto& a, const auto& b) { return a.first < b.first; };

   auto result3 = list3.
      Sort(IntMember_Comparer).                                         // sort by member n
      Select(&std::pair<int, char>::second).                            // select the char member
      ToQString();                                                      // Linq<QChar> and Linq<char> have member ToQString

   // result3 is "abcde"
}

void Maximum_Readability()
{
   // These lambda objects could also be pre-existing functions!
   auto IntIsEven = [](auto x) { return x % 2 == 0; };
   auto IntToString = [](auto x) { return QString::number(x); };

   auto result = CreateLinq<int>().
      FillRange(0, 10, 1).                                              // begin = 0; end(exclusive) = 10; increment = 1; --> {1, 2, 3, 4, 5, 6, 7, 8, 9}
      Where(IntIsEven).                                                 // To improve readability you can always pass in
      Select(IntToString);                                              // an address of an existing function or
                                                                        // a callable object
   // result is: {"0", "2", "4", "6", "8"}
}

void Example_Others()
{
   auto a1 = CreateLinq({1, 1, 2, 3, 4}).Left(2);                       // result: {1, 1}
   auto a2 = CreateLinq({"1", "2", "3", "4"}).Right(3);                 // result: {"2", "3", "4"}
   auto a3 = CreateLinq({1, 2, 3, 4}).GetRange(1, 2);                   // result: {2, 3}

   auto a4 = a1.Copy();                                                 // get a copy of the Linq object
   auto a6 = a4.Original();                                             // get the original data (std::vector<T>)
   auto a7 = a1.IsSameType(a2);                                         // if given Linq<T> and Linq<U> whether or not T and U are the same type --> false (int != const char*)
   auto a8 = a1.Add(1).AddAt(1, 1).Remove(1).RemoveAt(1);               // add and remove data on the fly
   auto a9 = a1.Merge(a3).Merge(a3, 3);                                 // merge multiple Linq objects

   auto b1 = a1[0];                                                     // access through index
   auto b3 = a1.size();                                                 // get the size of data vector
   a1 = CreateLinq({1, 1, 2, 3, 4});                                    // copy constructor
   for (auto& item : a1) {}                                            // iterate through Linq objects
   auto b4 = std::find(a1.begin(), a1.end(), 1);                        // begin and end iterators
   auto b5 = a1.ChangeType<float>().Average();                          // change type and basic math operations included
   auto b6 = a1.Median();                                               // basic math operations included
   auto b7 = a1.Sum();                                                  // basic math operations included
   auto b8 = a1.ChangeType<double>().Magnitude();                       // basic math operations included (considered T is convertible to double (int-> yes))
   auto b9 = a1.ChangeType<double>().Normalized();                      // basic math operations included (considered T is convertible to double (int-> yes))

   auto c1 = a1.Copy().Reverse();                                       // reverse operation
   auto c2 = a1.IndexOf(1);                                             // find first index matching this value
   auto c3 = a1.IndexOfRef(a1[2]);                                      // find first index which objects address matches param address
   auto c4 = c1.Last();                                                 // many first or last operations
   auto c4_1 = a1.AtOr(0, 8);
   auto c5 = c1.First();                                                // many first or last operations
   auto c6 = c1.LastOr(-1);                                             // many first or last operations
   auto c7 = c1.FirstOrThrow(std::exception("size: 0"));                // many first or last operations
   auto c8 = c1.FirstOrDefault();                                       // many first or last operations
   auto c9 = c1.LastOrDefault();                                        // many first or last operations

   auto d1 = Linq<int>() == Linq<int>();                                // equality operator
   auto d2 = Linq<int>() == Linq<double>();                             // also type independent
   auto d3 = a1.Count(1);                                               // count the number of occurrences
   a1.cend();
   a1.cbegin();
}

void Before_After()
{
   const auto FlooredToInt = [](auto x) { return int(std::floor(x)); };
   const auto ToQString = [](auto x) { return QString::number(x); };

   Linq<double> input = CreateLinq({1.2, 2.3, 3.1, 4.5, 5.8, 6.2});

   // we want to have an output list that contains the string of every even number in input when floored

   /*
      ##################### The 'Normal' way... #####################
   */

   std::vector<std::string> result1;                                    // 1
   for (auto item : input)                                              // 2
   {                                                                    // 3
      const int i_item = std::floor(item);                              // 4
      if (i_item % 2 == 0)                                              // 5
      {                                                                 // 6
         result1.emplace_back(std::to_string(i_item));                  // 7
      }                                                                 // 8
   }                                                                    // 9 lines of code

   /*
      ##################### VS Linq #####################
   */

   auto result2 = input.                                                // 1
      SelectIf(FlooredToInt, &IsEven).                                  // 2
      Select(ToQString);                                                // 3 lines of code

   // the results are the same! {2, 4, 6}
}