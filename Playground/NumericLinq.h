#pragma once
#include "LinqSpecializationMaker.h"
#include <complex>

namespace Common
{
   /*
      ##################### Specialization group #####################
   */
   LINQ_GROUP_BEGIN(NumericLinq);
   /// @brief 							get the sum of all elements in this list
   /// @note  							requirements:  T operator += (T)
   ///                                          default constructor
   T Sum() const
   {
      T res{};
      for (const T& item : ConstList())
         res = res + item;
      return res;
   }

   /// @brief 							get the average of all elements in this list
   /// @note  							uses function: Linq::Sum
   ///        							requirements:  int operator / (double)
   T Average() const
   {
      if (ConstList().size() == 0)
         return 0;

      return Sum() / static_cast<double>(ConstList().size());
   }

   /// @brief 							get the median of all elements in this list
   template<typename FNC>
   T Median(FNC comp) const
   {
      return ConstThis().Copy().Sort(comp)[ConstList().size() / 2];
   }

   /// @brief 							get the median of all elements in this list
   T Median() const
   {
      return ConstThis().Copy().Sort()[ConstList().size() / 2];
   }

   /// @brief 							get the magnitude from this list
   /// @note  							requirements:  default constructor
   ///                                          int operator*(int)
   ///                                          int operator+(int)
   ///                                          std::sqrt(int)
   T Magnitude() const
   {
      T res{};
      for (const T& item : ConstList())
         res += item * item;
      return std::sqrt(res);
   }

   /// @brief 							get the list in normalized form
   /// @note  							uses function: Linq::Magnitude
   ///                           creates NO copy! returns itself
   Linq<T>& Normalized()
   {
      T mag = Magnitude();

      if (mag == 0)
         return This();

      for (T& item : List())
         item /= mag;

      return This();
   }

   LINQ_GROUP_END(NumericLinq);

   /*
      ##################### Specializations #####################
   */

   LINQ_SPECIALIZATION(int, NumericLinq);
   LINQ_SPECIALIZATION(unsigned, NumericLinq);
   LINQ_SPECIALIZATION(float, NumericLinq);
   LINQ_SPECIALIZATION(double, NumericLinq);
   LINQ_SPECIALIZATION(long double, NumericLinq);
   LINQ_SPECIALIZATION(long, NumericLinq);
   LINQ_SPECIALIZATION(long long, NumericLinq);
   LINQ_SPECIALIZATION(unsigned long long, NumericLinq);

   template<typename T> LINQ_TEMPLATE_SPECIALIZATION(std::complex<T>, NumericLinq);
}