#pragma once
#include "LinqSpecializationMaker.h"

namespace Common
{

   LINQ_GROUP_BEGIN(BaseLinq2D);

   Linq<T> AsOneLinq() const
   {
      auto LinqAppender = [](auto& linq, const auto& value) { linq.Add(value); };
      return ConstThis().Accumulate(Linq<T>(), LinqAppender);
   }

   LINQ_GROUP_END(BaseLinq2D);

   template<typename T> LINQ_TEMPLATE_SPECIALIZATION(Linq<T>, BaseLinq2D);
   template<typename T> using Linq2D = Linq<Linq<T>>;

}