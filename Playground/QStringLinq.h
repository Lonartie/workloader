#pragma once
#include "LinqSpecializationMaker.h"

#include <QtCore/QString>
#include <QtCore/QStringList>

namespace Common
{
   /*
      ##################### Specialization group #####################
   */

   LINQ_GROUP_BEGIN(QStringLinq);

   /// @brief 							get the elements as QStringList
   QStringList ToQStringList() const
   {
      return ConstThis().Accumulate(QStringList(), &QStringLinq::QStringListAccumulator);
   }

   /// @brief 							sums up a QString from begin to end
   QString Concatenate() const
   {
      return ConstThis().Accumulate(QString(), &QStringLinq::QStringSum);
   }

   Linq<int> ToIntLinq() const
   {
      return ConstThis().Select(&QStringLinq::To<int>);
   }

   Linq<double> ToDoubleLinq() const
   {
      return ConstThis().Select(&QStringLinq::To<double>);
   }

   Linq<float> ToFloatLinq() const
   {
      return ConstThis().Select(&QStringLinq::To<float>);
   }

   static void QStringListAccumulator(QStringList& list, const QString& str)
   {
      list.append(str);
   }

   static void QStringSum(QString& out, const QString& item)
   {
      out += item;
   }

private:

   template<typename U> static U To(const QString& str) { return U{}; }
   template<> static int To<int>(const QString& str) { return str.toInt(); }
   template<> static double To<double>(const QString& str) { return str.toDouble(); }
   template<> static float To<float>(const QString& str) { return str.toFloat(); }

   LINQ_GROUP_END(QStringLinq);

   /*
      ##################### Specializations #####################
   */

   LINQ_SPECIALIZATION(QString, QStringLinq);
}