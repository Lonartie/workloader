#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(WLCOMMON_LIB)
#  define WLCOMMON_EXPORT Q_DECL_EXPORT
# else
#  define WLCOMMON_EXPORT Q_DECL_IMPORT
# endif
#else
# define WLCOMMON_EXPORT
#endif
