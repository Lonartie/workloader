#pragma once

#include "stdafx.h"
#include "WLObject.h"
#include "Conversion.h"

namespace WL
{
   namespace Common
   {
      template<typename T>
      struct Serializable
      {
         Serializable(T* This): This(This), Size(sizeof(T)) {}

         void Save(const QString& path)
         {
            QFile file(path);
            file.open(QIODevice::WriteOnly);
            QByteArray data(Conversion::ToRaw(This), Size);
            file.write(data);
            file.flush();
            file.close();
         }

         QByteArray Serialize()
         {
            return QByteArray(Conversion::ToRaw(This), Size);
         }

         static T Load(const QString& path)
         {
            QFile file(path);
            file.open(QIODevice::ReadOnly);
            auto dat = file.readAll();
            file.close();
            T result;
            auto raw = dat.data();
            auto rres = &result;
            memcpy(rres, raw, sizeof(T));
            return result;
         }

         static T& Deserialize(const QByteArray& data)
         {
            return *(Conversion::FromRaw<T>(data.data()));
         }

      private:

         T* This;
         std::size_t Size;
      };
   }
}