#pragma once

#include "stdafx.h"

#include <QtCore/QObject>

namespace WL
{
   namespace Common
   {
      class WLCOMMON_EXPORT WLObject : public QObject
      {
         Q_OBJECT
         QOBJECT_DEFINITION(WLObject);

      public:
         
         ~WLObject() = default;

         QObject* behind = nullptr;
      };
   }
}