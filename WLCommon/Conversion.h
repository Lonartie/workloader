#pragma once

namespace WL
{
   namespace Common
   {
      namespace Conversion
      {
         template<typename T>
         char* ToRaw(T* dat)
         {
            return static_cast<char*>(static_cast<void*>(dat));
         }

         template<typename T>
         const char* ToConstRaw(const T* dat)
         {
            return static_cast<const char*>(static_cast<const void*>(dat));
         }

         template<typename T>
         T* FromRaw(char* dat)
         {
            return static_cast<T*>(static_cast<void*>(dat));
         }

         template<typename T>
         const T* FromConstRaw(const char* dat)
         {
            return static_cast<const T*>(static_cast<const void*>(dat));
         }

         unsigned constexpr const_hash(const char* input) 
         {
            return *input ?
               static_cast<unsigned int>(*input) + 33 * const_hash(input + 1) :
               5381;
         }
      }
   }
}