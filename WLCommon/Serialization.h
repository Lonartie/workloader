#pragma once

#include "stdafx.h"
#include "WLObject.h"

namespace WL
{
   namespace Common
   {
      TEMPLATE_T class WLCOMMON_EXPORT WLSerializable : public QObject, public WLObject
      {
         Q_OBJECT

         QOBJECT_DEFINITION(WLSerializable);

         void RawSerializeTo(const QString & path);

         static T RawSerializeFrom(const QString & path);

      private:

         WLObject* child = nullptr;

         uchar* child_uraw = nullptr;

         char* child_raw = nullptr;

         uint64_t child_size = 0;
      };
   }
}

TEMPLATE_T WL::Common::WLSerializable<T>::WLSerializable(QObject* parent)
   : QObject(parent), WLObject(this)
{
   T* child = reinterpret_cast<T*>(parent);

   if (child != nullptr)
   {
      delete this;
   }

   child_size = sizeof(T);

   child_uraw = static_cast<uchar*>(child);
   child_raw = reinterpret_cast<char*>(child_uraw);

   this->child = child;
}

TEMPLATE_T void WL::Common::WLSerializable<T>::RawSerializeTo(const QString& path)
{
   QFile _file(path);
   QByteArray _data(child_raw);

   if (!_file.open(QIODevice::WriteOnly))
      return;

   _file.write(_data);
   _file.flush();
   _file.close();
}

TEMPLATE_T T WL::Common::WLSerializable<T>::RawSerializeFrom(const QString& path)
{
   QFile _file(path);

   if (!_file.open(QIODevice::ReadOnly))
      return nullptr;

   QByteArray _data(_file.readAll());
   char* _raw = _data.data();

   if (_data.size() != sizeof(T))
      return nullptr;

   T _t {};
   char* _t_raw = static_cast<char*>(&_t);

   for (int n = 0; n < sizeof(T); n++)
      _t_raw[n] = _raw[n];

   return _t;
}
