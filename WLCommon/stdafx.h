#pragma once

#include "CommonIncludes.h"
#include "CommonMacros.h"

#include "wlcommon_global.h"

/*
	#################### init ####################
*/

WLCOMMON_EXPORT bool __common_init_lib();