#pragma once

/*
	#################### GENERAL ####################
*/

#define STRING(x) #x
#define STRING2(x) STRING(x)

#define INDIRECT(x) x
#define INDIRECT2(x) INDIRECT(x)

#define CAT(a, b) a##b
#define CAT2(a, b) CAT(a, b)

#define offset_d(i,f)    (long(&(i)->f) - long(i))
#define offset_s(t,f)    offset_d((t*)1000, f)

/*
   #################### WARNINGS ####################
*/

#define WL_SUPPRESS_WARNINGS_BEGIN __pragma(warning(push, 0))
#define WL_SUPPRESS_WARNINGS_END __pragma(warning(pop))

/*
   #################### QObject ####################
*/

//WL_SUPPRESS_WARNINGS_BEGIN
#include <QObject>
//WL_SUPPRESS_WARNINGS_END
#include <memory>

#define SHARED_PTR_CLASS(class_name) public: using SPtr = std::shared_ptr<class_name>; template<typename ...T>static SPtr CreateShared(T...args) { return std::make_shared<class_name>(args...); }
#define UNIQUE_PTR_CLASS(class_name) public: using UPtr = std::unique_ptr<class_name>; template<typename ...T>static UPtr CreateUnique(T...args) { return std::make_unique<class_name>(args...); }
#define MEMORY(class_name) SHARED_PTR_CLASS(class_name); UNIQUE_PTR_CLASS(class_name)
#define QOBJECT_DEFINITION(class_name) MEMORY(class_name) public: class_name (QObject* parent = nullptr);
#define QOBJECT_IMPLEMENTATION(class_name) class_name :: class_name (QObject* parent) : WLObject(this) { }

#define WL_CLASS_EXPORT(export_macro, class_name) class export_macro class_name : public QObject, public WL::Common::WLObject
#define WL_CLASS(class_name) WL_CLASS_EXPORT(class_name, )
#define WL_IMPLEMENTS(...) , __VA_ARGS__

#define TEMPLATE_T template<typename T>
