#include <boost/test/unit_test.hpp>

#include "../WLCommon/WLSerializable.h"

#include <QFile>

using namespace WL::Common;

struct _test: public Serializable<_test>
{
   _test(): Serializable<_test>(this) {}
   int a;
};

struct _test2: public Serializable<_test2>
{
   _test2(): Serializable<_test2>(this) {}
   int a;
};

BOOST_AUTO_TEST_SUITE(SerializationTests)

BOOST_AUTO_TEST_CASE(SerializableCanSave)
{
   BOOST_CHECK_EQUAL(false, QFile::exists("SerializableCanSave_test.bin"));
   BOOST_CHECK_EQUAL(false, QFile::exists("SerializableCanSave_test2.bin"));

   _test test;
   test.a = 20;
   test.Save("SerializableCanSave_test.bin");

   _test2 test2;
   test2.a = 20;
   test2.Save("SerializableCanSave_test2.bin");

   BOOST_CHECK_EQUAL(true, QFile::exists("SerializableCanSave_test.bin"));
   BOOST_CHECK_EQUAL(true, QFile::exists("SerializableCanSave_test2.bin"));

   QFile::remove("SerializableCanSave_test.bin");
   QFile::remove("SerializableCanSave_test2.bin");

   BOOST_CHECK_EQUAL(false, QFile::exists("SerializableCanSave_test.bin"));
   BOOST_CHECK_EQUAL(false, QFile::exists("SerializableCanSave_test2.bin"));
}

BOOST_AUTO_TEST_CASE(SerializableCanLoad)
{
   _test test;
   test.a = 20;
   test.Save("SerializableCanLoad_test.bin");

   _test2 test2;
   test2.a = 30;
   test2.Save("SerializableCanLoad_test2.bin");

   _test res = _test::Load("SerializableCanLoad_test.bin");
   _test2 res2 = _test2::Load("SerializableCanLoad_test2.bin");

   BOOST_CHECK_EQUAL(20, res.a);
   BOOST_CHECK_EQUAL(30, res2.a);

   QFile::remove("SerializableCanLoad_test.bin");
   QFile::remove("SerializableCanLoad_test2.bin");
}

BOOST_AUTO_TEST_SUITE_END();